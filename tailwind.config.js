/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        ash: {
          black: "#2E3440",
          blue: {
            DEFAULT: "#5E81AC",
            dark: "#2864AA",
            light: "#81A1C1",
          },
          gray: {
            dark: "#435C5E",
            light: "#D8DEE9",
          },
          purple: {
            DEFAULT: "#B48EAD",
            light: "#DAA9D1",
          },
          red: {
            DEFAULT: "#BF616A",
            light: "#DE939B",
          },
          white: "#ECEFF3",
          yellow: {
            DEFAULT: "#EBCB8B",
            light: "#F5DBA6",
          },
        },
      },
      fontFamily: {
        display: ["Fjalla One", "sans"],
        mono: ["Fira Mono", "mono"],
        sans: ["Montserrat", "sans"],
        serif: ["Bitter", "serif"],
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
}
