export default defineAppConfig({
  nuxtIcon: {
    size: "24px", // default <Icon> size applied
    class: "icon", // default <Icon> class applied
    aliases: {
      codepen: "fa6-brands:codepen",
      close: "ic:round-close",
      github: "fa6-brands:github",
      gitlab: "fa6-brands:gitlab",
      linkedin: "fa6-brands:linkedin",
      menu: "fa6-solid:bars-staggered",
      "toggle-off": "fa6-solid:toggle-off",
      "toggle-on": "fa6-solid:toggle-on",
    },
  },
});
