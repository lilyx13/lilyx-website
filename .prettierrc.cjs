/** @type {import("prettier").Options} */
const config = {
  trailingComma: "es5",
  tabWidth: 2,
  semi: true,
  singleQuote: false,
  plugins: [require("prettier-plugin-tailwindcss")],
};

module.exports = config;
